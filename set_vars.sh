#!/bin/bash

if [ ! -z $PANKISYNCD_CONF ];
then
    conf=$PANKISYNCD_CONF
else
    conf=pankisyncd.conf
fi

# Convert config values to env vars but only if each value is not already set
while read -r line; do
    # If comment or section header or empty line then continue
    ([[ "$line" =~ ^[[:blank:]]*#.*$ ]] || [[ "$line" =~ ^[[] ]] || [[ "$line" =~ ^[[:blank:]]*$ ]]) && continue

    # Capture the values from the config file and dynamically set ANKISYNCD_ env vars, if not already set
    if [[ "$line" =~ ^[[:blank:]]*([^[:blank:]]*)[[:blank:]]*=[[:blank:]]*([^[:blank:]]*)[[:blank:]]*$ ]];
    then
        var="ANKISYNCD_${BASH_REMATCH[1]^^}"
        # do not override from file a variable that has already been set
        [ ! -z ${!var+x} ] && continue
        export "ANKISYNCD_${BASH_REMATCH[1]^^}"="${BASH_REMATCH[2]}"
    fi
done < "$conf"

# we want postgres, so empty the default sqlite3 managers, unless explicitly set
[ -z ${ANKISYNCD_AUTH_DB_PATH+x} ] && export ANKISYNCD_AUTH_DB_PATH=
[ -z ${ANKISYNCD_SESSION_DB_PATH+x} ] && export ANKISYNCD_SESSION_DB_PATH=

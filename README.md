pg-ankisyncd
============

`pg-ankisyncd` is a PostgreSQL compatibility layer on top of [ankisyncd](https://github.com/tsudoko/anki-sync-server). `ankisyncd` is included in this project as a submodule. `ankisyncd` includes [Anki](https://github.com/dae/anki) itself as a submodule, so be aware that a `git submodule update --init --recursive`, will fetch `Anki` also.

See [ankisyncd](https://github.com/tsudoko/anki-sync-server) for project history and more descriptive setup help.

Installation
============
```
$ pip install -r requirements.txt  # requirements.dev.txt for running the tests
```
You need a postgres server and user with permissions to create schemas and tables in the DB.

Running `pg-ankisyncd`
======================

Update `ankisyncd.conf` in the top-most directory with your database connection info. You can then simply run:
```
$ ./runserver.sh
```
And it will start a server listening on the default address (127.0.0.1) and port (27701). You can also provide your own config file:
```
$ ./runserver.sh myconfig.conf
```
To provide your own (non-versioned) file.
Notes
=====

`pg-ankisyncd` currently only supports using postgres for the main user data databases, not the media dbs. Work is on-going and support for the media dbs will arrive shortly. Support for storing the media files in postgres may be considered at some stage, though there appears little value of doing this rather than simply using connected network storage.

`pg-ankisyncd` is used by [Transcrobes](https://transcrob.es) to provide an Anki-compatible server backend and as a data store for user data.

#!/bin/bash

export PYTHONPATH=$PYTHONPATH:panki:asd:asd/ankisyncd/anki-bundled

if [ ! -z $1 ];
then
    PANKISYNCD_CONF=$1
fi

source set_vars.sh $PANKISYNCD_CONF

python -m ankisyncd
